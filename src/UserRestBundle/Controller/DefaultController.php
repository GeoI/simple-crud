<?php

namespace UserRestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function getAction()
    {
        return $this->render('UserRestBundle:Default:index.html.twig');
    }


}
