<?php

namespace UserRestBundle\Controller;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use UserRestBundle\Entity\DailyActiveUsers;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * DailyActiveUsers controller.
 *
 * @Route("user-statistic")
 */
class DailyActiveUsersController extends ApiController
{

    /**
     * Lists all dailyActiveUser entities.
     * Query examples: <br>
     * GET /user-statistic/?fromDate=2017-02-04 00:35:00&toDate=2017-02-04 01:18:32 <br>
     * GET /user-statistic/?fromDate=2017-02-04 00:35:00 <br>
     *     return json:<br>{ Users } or ,  <br>    {errors:[{'message':'...']}}
     *
     * @ApiDoc(
     * resource=true,
     *  section="Daily user activity",
     *  statusCodes={
     *      200="Returned when successful",
     *      400="Returned when the invalid requested date format"
     *  },
     * filters = {
     *      {"name"="fromDate", "datatype"="string", "requirements"="Y-m-d H:i:s", "required"="false"},
     *      {"name"="toDate", "datatype"="string", "requirements"="Y-m-d H:i:s", "required"="false"}
     *      },
     * input="UserRestBundle\Type\UserType",
     * )
     *
     * @Route("/", name="user-statistic_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $from = $request->query->get('fromDate');
        $to = $request->query->get('toDate');

        $response = ['errors' => []];
        $statusCode = self::CODE_SUCCESS;

        $dateFrom = \DateTime::createFromFormat('Y-m-d H:i:s', $from);

        if ($from && !$dateFrom) {
            $statusCode = self::CODE_BAD_REQUEST;
            $response['errors'][] = ['message' => 'Wrong \'from\' format, \'Y-m-d H:i:s\' required.'];
        }

        $dateTo = \DateTime::createFromFormat('Y-m-d H:i:s', $to);
        if ($to && !$dateTo) {
            $statusCode = self::CODE_BAD_REQUEST;
            $response['errors'][] = ['message' => 'Wrong \'to\' format, \'Y-m-d H:i:s\' required.'];
        }

        if ( count($response['errors']) == 0 ) {
            $rows = $this->getRepository()->findUniqueByDates($dateFrom, $dateTo);
            $response = $this->container->get('serializer')->serialize($rows, 'json');
        }

        return new JsonResponse($response, $statusCode);
    }

    /**
     * Creates a new dailyActiveUser entity.
     *
     * @QueryParam(
     *      name="userId",
     *      requirements="int",
     *      strict=true,
     *      nullable=false,
     *      description="User id"
     * )
     *
     * @ApiDoc(
     * section = "Daily user activity",
     * statusCodes={
     *      201="Returned when successful",
     *      400={
     *          "Returned when the relating user is not found",
     *          "Returned when something else is wrong"
     *          },
     * })
     *
     * @Route("/", name="user-statistic_new")
     * @Method({"POST"})
     *
    */
    public function newAction(Request $request)
    {
        $user = new DailyActiveUsers();
        $form = $this->createForm('UserRestBundle\Form\DailyActiveUsersType', $user);
        $form->handleRequest($request);

        $statusCode = self::CODE_BAD_REQUEST;
        $response = ['errors' => []];

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            try {
                $em->flush($user);
                $statusCode = self::CODE_ENTITY_CREATED;
                $response = ['id' => $user->getUserId()];
            } catch (\Exception $e) {
                if (strpos($e->getMessage(), 'CONSTRAINT')) {
                    $response['errors'][] = ['message' => 'User not found'];
                } else {
                    $response['errors'][] = ['message' => 'Database error'];
                }
            }
        } else {
            foreach($form->getErrors(true) as $e) {
                $response['errors'][] = ['message' => $e->getMessage()];
            }
        }

        return new JsonResponse($response, $statusCode);
    }

    /**
     *
     * @return \UserRestBundle\Repository\DailyActiveUsersRepository
     */
    private function getRepository()
    {
        $em = $this->get('doctrine.orm.entity_manager');
        return $em->getRepository('UserRestBundle:DailyActiveUsers');
    }
}
