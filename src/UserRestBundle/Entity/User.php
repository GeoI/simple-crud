<?php

namespace UserRestBundle\Entity;

use Doctrine\Common\Annotations\Annotation\Required;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="UserRestBundle\Repository\UserRepository")
 */
class User
{

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=50, unique=true)
     * @NotBlank(message="Login can not be blank")
     * @Length(min="3", max="50",
     * minMessage = "Login value must be at least {{ limit }} characters long",
     *      maxMessage = "Login value cannot be longer than {{ limit }} characters"))
     * @Regex(
     *      pattern="/^[a-zA-Z0-9]+$/",
     *     match=true,
     *     message="Login property should match [a-zA-Z0-9] without spaces")
     */
    private $login;

    /**
     * @var string
     * @Length(min="1", max="250")
     * @NotBlank(message="Name can not be blank")
     * @ORM\Column(name="name", type="string", length=250)
     */
    private $name;


    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return User
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set login
     *
     * @param string $login
     *
     * @return User
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function toArray(){
        return [
            'id' => $this->getUserId(),
            'name' => $this->getName(),
            'login' => $this->getLogin(),
        ];
    }
}

