<?php

namespace UserRestBundle\Controller;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use UserRestBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * User controller.
 *
 * @Route("user")
 */
class UserController extends ApiController
{

    /**
     *
     * Lists all user entities.
     *
     * @ApiDoc(
     *  section="User CRUD",
     *  statusCodes={
     *      200="Returned when successful"
     *  },
     *  output="UserRestBundle\Entity\User"
     * )
     *
     * @Route("/", name="user_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $users = $this->getRepository()->findAll();
        $serializedEntity = $this->container->get('serializer')->serialize($users, 'json');

        return new JsonResponse($serializedEntity, self::CODE_SUCCESS);
    }

    /**
     * Finds and return user entity.
     *
     * @ApiDoc(
     *  section="User CRUD",
     *  statusCodes={
     *      200="Returned when successful",
     *      404="Returned when User not found",
     *  },
     * )
     * @Route("/{user}", name="user_show", requirements={"user": "\d+"})
     * @Method("GET")
     */
    public function showAction(Request $request)
    {
        $statusCode = self::CODE_NOT_FOUND;
        $serializedEntity = [];

        $entity = $this->getRepository()->find($request->get('user'));
        if ($entity) {
            $statusCode = self::CODE_SUCCESS;
        }

        return new JsonResponse($entity->toArray(), $statusCode);
    }

    /**
     * Creates a new user entity.
     *
     * @ApiDoc(
     * section="User CRUD",
     *  statusCodes={
     *      201="Returned when user was created",
     *      400="Returned when User with same login already exists and other errors",
     *  },
     * )
     *
     * @QueryParam(
     *      name="login",
     *      requirements="string min 3, max 50",
     *      strict=true,
     *      nullable=false,
     *      description="Unique User login"
     * )
     *
     * @QueryParam(
     *      name="name",
     *      requirements="string min 1, max 250",
     *      strict=true,
     *      nullable=false,
     *      description="User name",
     * )
     *
     * @Route("/", name="user_new")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function newAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm('UserRestBundle\Form\UserType', $user);
        $form->handleRequest($request);

        $statusCode = self::CODE_BAD_REQUEST;
        $response = ['data' => [], 'errors' => []];

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            try {
                $em->flush($user);
                $statusCode = self::CODE_ENTITY_CREATED;
                $response['data'] = ['id' => $user->getUserId()];
            } catch (UniqueConstraintViolationException $e) {
                $response['errors'][] = 'User with login \'' . $request->request->get('login') . '\' already exists';
            } catch (\Exception $e) {
                $response['errors'][] = 'Database error ';
            }
        } else {
            foreach($form->getErrors(true) as $e) {
                $response['errors'] = $e->getMessage();
            }
        }

        return new JsonResponse($response, $statusCode);
    }

    /**
     * Updates an existing user entity.
     *
     * @ApiDoc(
     * section="User CRUD",
     *  statusCodes={
     *      200="Returned when user was created",
     *      400="Returned when some database error, login too short or invalid",
     *  },
     *  output="UserRestBundle\Entity\User"
     * )
     *
     * @QueryParam(
     *      name="login",
     *      requirements="string min 3, max 50",
     *      strict=true,
     *      nullable=false,
     *      description="Unique User login"
     * )
     *
     * @QueryParam(
     *      name="name",
     *      requirements="string min 1, max 250",
     *      strict=true,
     *      nullable=false,
     *      description="User name",
     * )
     *
     * @Route("/{user}", name="user_edit", requirements={"user": "\d+"})
     * @Method({"PUT"})
     *
     * @return JsonResponse
     */
    public function editAction(Request $request)
    {
        $user = $this->getRepository()->find($request->get('user'));

        $editForm = $this->createForm('UserRestBundle\Form\UserType', $user, ['method' => 'PUT']);
        $editForm->handleRequest($request);

        $statusCode = self::CODE_BAD_REQUEST;
        $response = ['errors' => []];

        if (!$user) {
            return new JsonResponse(['errors' => ['message' => 'User not found']], self::CODE_NOT_FOUND);
        }

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $this->getDoctrine()->getManager()->persist($user);
            try {
                $this->getDoctrine()->getManager()->flush($user);
                $statusCode = self::CODE_SUCCESS;
                $response = $this->container->get('serializer')->serialize($user, 'json');
            } catch (\Exception $e) {
                $response['errors'][] = ['message' => 'Database error'];
            }

        }
        else if (!$editForm->isSubmitted())
        {
            $response['errors'][] = ['message' => 'Shoud use x-www-form-urlencoded'];
        }
        else
        {
            foreach($editForm->getErrors(true) as $e) {
                $response['errors'][] = ['message' => $e->getMessage()];
            }
        }

        return new JsonResponse($response, $statusCode);
    }

    /**
     * Deletes a user entity.
     *
     * @ApiDoc(
     * section="User CRUD",
     *  statusCodes={
     *      200="Returned when user was deleted successfully",
     *      400="Returned when some database error",
     *      404="Returned when User not found",
     *  },
     * )
     *
     * @Route("/{user}", name="user_delete", requirements={"user": "\d+"})
     * @Method("DELETE")
     * @return JsonResponse
     */
    public function deleteAction(Request $request)
    {
        $user = $this->getRepository()->find($request->get('user'));
        $response = ['errors' => []];

        if ($user) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            try {
                $em->flush($user);
                $response = ['userId' => $request->get('user')];
                $statusCode = self::CODE_SUCCESS;
            } catch (\Exception $e) {
                $response['errors'][] = ['message' => 'Database error'];
                $statusCode = self::CODE_BAD_REQUEST;
            }
        } else {
            $statusCode = self::CODE_NOT_FOUND;
            $response['errors'][] = ['message' => 'User \'' . $request->get('user') . '\' not found'];
        }

        return new JsonResponse($response, $statusCode);
    }

    /**
     *
     * @return \UserRestBundle\Repository\UserRepository
     */
    private function getRepository()
    {
        $em = $this->get('doctrine.orm.entity_manager');
        return $em->getRepository('UserRestBundle:User');
    }
}
