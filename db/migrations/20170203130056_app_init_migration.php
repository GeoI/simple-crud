<?php

use Phinx\Migration\AbstractMigration;

class AppInitMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {

        $table = $this->table('user', ['id' => false, 'primary_key' => 'user_id', 'identity' => true]);
        $table
            ->addColumn('user_id', 'integer', ['identity' => true])

            ->addColumn('login', 'string', ['limit' => 50])
            ->addIndex('login', ['unique' => true])

            ->addColumn('name', 'string',  ['limit' => 250])
            ->create();

        $table = $this->table('daily_active_users', ['id' => false, 'primary_key' => 'user_id', 'identity' => false]);
        $table
            ->addColumn('user_id', 'integer')
            ->addForeignKey('user_id', 'user', 'user_id', ['delete'=>'CASCADE', 'update'=>'CASCADE'])

            ->addColumn('date',  'timestamp', ['default'=>'CURRENT_TIMESTAMP'])
            ->addIndex('date')
            ->create();

    }
}
