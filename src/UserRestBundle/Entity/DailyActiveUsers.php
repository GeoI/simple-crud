<?php

namespace UserRestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * DailyActiveUsers
 *
 * @ORM\Table(name="daily_active_users")
 * @ORM\Entity(repositoryClass="UserRestBundle\Repository\DailyActiveUsersRepository")
 */
class DailyActiveUsers
{

    public function __construct()
    {
        $this->date = new \DateTime();
    }

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     * @NotBlank()
     */
    private $userId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return DailyActiveUsers
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return DailyActiveUsers
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
}

