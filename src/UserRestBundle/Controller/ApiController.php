<?php

namespace UserRestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Api base controller
 *
 */
class ApiController extends Controller
{
    const CODE_SUCCESS = 200;
    const CODE_ENTITY_CREATED = 201;
    const CODE_BAD_REQUEST = 400;
    const CODE_NOT_FOUND = 404;
}
